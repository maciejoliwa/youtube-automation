# YouTube Automation Tool

Application to automate upload of YouTube videos.

### How it works:
1. Application detects new videos in a given directory,
2. It logins to YouTube, and starts uploading said videos, setting proper metadata (visibility, monetization)
3. After the video's visibility is set to **Unlisted**, it adds a record to the *Airtable* database and to the cache.
4. After the app finished processing all of the videos in directory, it will continiously check the same directory, until it finds new videos.

### Technologies:
* Node.js,
* Airtable.js,
* Dotenv,
* Playwright,
* TypeScript,
* TS-Node

### How to run:
If you're running it for the first time:
```
> npm i
> npm --max-old-space-size=4096 start
```

And later you can just use:

```
> npm --max-old-space-size=4096 start
```
