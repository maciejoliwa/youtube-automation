import 'dotenv/config';

import Airtable from 'airtable';
import { parse } from 'path';

export enum AirtableOperationStatus {
    Success,
    Error
};

Airtable.configure({
    apiKey: process.env["AIRTABLE_KEY"]
});
const base = Airtable.base('appZ6xrU3emUyZ3KE');

export function isInAirTable(filename: string, table: string = "Videos"): Promise<boolean> {
    return new Promise((resolve, reject) => {
        base(table).select({
            fields: ["Filename"],
            maxRecords: 500,
            view: "Grid view",
            filterByFormula: `{Filename} = '${filename}'`
        }).eachPage((records, nextPageFunction) => {
            
            if (records.length > 0) {
                resolve(true);
            } else {
                resolve(false);
            }
            
            nextPageFunction();
        }, (error) => {
            if (error) {console.log(error); }
            resolve(false);
        });
    
    })
}

export function addRecord(filepath: string, url: string | null | undefined): AirtableOperationStatus {
    const filename = parse(filepath).name;
    if (url === null || url === undefined) {
        return AirtableOperationStatus.Error;
    }

    base('Videos').create([
        {
            fields: {
                Filename: filepath,
                Assignee: "",
                Title: filename,
                Description: "",
                Status: "Todo",
                UploadDate: new Date().toJSON().slice(0,10).replace(/-/g,'-'),
                Priority: "Low",
                URL: url as string,
            }
        }
    ], (err, _) => {
        if (err) {console.log(err); return AirtableOperationStatus.Error};

        return AirtableOperationStatus.Success;
    });

    return AirtableOperationStatus.Error;
}