import 'dotenv/config';

import { Page, webkit, firefox, Browser, BrowserContext } from 'playwright';
import { readdir, rename } from 'fs/promises';
import { addRecord, isInAirTable } from './airtable';
import { UnlistedStatusFixer } from './unlistedFix';
import login from './login';

const statusFixer = new UnlistedStatusFixer();
const version = "1.0.1";
const greeting = `
_                      _                          _   
| |_    __ _   _ _   __| |  _ _   ___   ___  ___  | |_ 
| ' \\  / _\` | | '_| / _\` | | '_| / -_) (_-< / -_) |  _|
|_||_| \\__,_| |_|   \\__,_| |_|   \\___| /__/ \\___|  \\__|                           
`;                                                   

console.log(greeting)
console.log(`Version: ${version}`);
console.log("Youtube Uploader")

// Tutaj pełna ścieżka do miejsca, które ma się synchronizować np "C:/Moje\ Dokumenty/YouTube". Zależna od zmiennej środowiskowej DEBUG_MODE
let DIR: string = process.env["DEBUG_MODE"] === "1" ? 'Y:/YouTube/Maciej O Testy/Upload/' : 'Z:/YouTube/AutoUpload/';

const STUDIO_CHANEL_URL =
'https://studio.youtube.com/channel/UCBviUkaeHb3SQPdtIRTNtJQ/videos/upload'  // OFISZIAL
    // "https://studio.youtube.com/channel/UCeIIBVKrJMRKyRT2WpJcEmA/videos/upload"    
    // 'https://studio.youtube.com/channel/UCLHMs6GoCv7YgsF3ZMxnf8A/videos/upload'; // Tutaj jest URL tego kanału TESTY
const LOGIN = process.env["YT_EMAIL"]; // adres email do konta Google
const PASSWORD = process.env["YT_PASSWORD"]; // hasło do konta Google - można zostawić puste - będzie możliwość wpisania przy logowaniu

let browser: Browser;
let context: BrowserContext;
let page: Page;

// Logs how much memory is used (in MB)
function logMemoryUsed() {
    console.log(`Used: ${Math.floor(process.memoryUsage().heapUsed / 1024 / 1024)}`);
}

/*
    Grabs a path to an .mp4 file and uploads it to YouTube.
    Returns true, if the upload was successfull, false otherwise.
*/
async function uploadVideo(filepath: string): Promise<boolean> {
    try {
        context = await browser.newContext({ storageState: "state.json" });
        page = await context.newPage();
        let videoURL: string | null | undefined;  // This will hold the URL of the uploaded video
        await page.goto(STUDIO_CHANEL_URL, { timeout: 1000 * 5000 });
        await page.waitForTimeout(2000);
        await page.click('ytcp-button[label=Utwórz]');
        await page.click('#text-item-0');
        await page.setInputFiles('input[type="file"]', []);
        await page.setInputFiles('input[type="file"]', `${DIR}/${filepath}`, { timeout: 5000 });
        
        await page.waitForTimeout(500).then(async () => {   
            while (true) {
                const elementContent = await (await page.$('.ytcp-uploads-dialog .progress-label'))?.textContent() 
                // @ts-ignore
                if (
                    elementContent?.trim() === "Przetwarzam: HD" ||
                    elementContent?.trim() === "Weryfikacja została zakończona. Nie znaleziono problemów."
                ) {
                    break;
                }
            }
        })

        await page.waitForSelector('tp-yt-paper-radio-button[name="VIDEO_MADE_FOR_KIDS_NOT_MFK"]', { timeout: 50000 });
        await page.click('tp-yt-paper-radio-button[name="VIDEO_MADE_FOR_KIDS_NOT_MFK"]');
        videoURL = await (await page.waitForSelector('a.ytcp-video-info'))?.textContent();
        await page.waitForSelector('ytcp-button#next-button');
        await page.click('ytcp-button#next-button');
        await page.click('ytcp-form-input-container#m10n-container');
        await page.click(
            '.ytcp-video-monetization-edit-dialog tp-yt-paper-radio-button#radio-on'
        );
        await page.click(
            '.ytcp-video-monetization-edit-dialog ytcp-button#save-button'
        );
        await page.click('ytcp-button#next-button');
        await page.click('.all-none-checkbox');
        await page.waitForTimeout(1000);
        await page.waitForSelector('#submit-questionnaire-button', { timeout: 10000 });
        await page.click('#submit-questionnaire-button').then(async () => {
            await page.click('ytcp-button#next-button');
        });
        // Some videos are too short for video files to be added at the very end, so in case of uplading a very short video, we just don't take this step
        try {
            await page.waitForSelector('ytcp-button[label="Importuj z filmu"]', { timeout: 15000 });
            await page.click('ytcp-button[label="Importuj z filmu"]', { timeout: 15000 });
            await (await page.waitForSelector('ytcp-button#import-endscreen-from-video-button')).click();
            await (await page.waitForSelector('input#search-yours')).fill('https://www.youtube.com/watch?v=dY6kVjsHDmM&t=16s&ab_channel=HardReset.Info').then(async () => {
                await page.waitForTimeout(5000);    
                await page.waitForSelector('ytcp-entity-card', { timeout: 10000 }).then(async card => await card.click());
            });
            await (await page.waitForSelector('#save-container')).click();
        }
        catch {
            console.log("Film za krótki, aby dodać rekomendowane filmy na końcu");
        }
        await page.waitForTimeout(1500);
        await page.click('ytcp-button#next-button', { timeout: 30000 });
        await page.click('ytcp-button#next-button', { timeout: 30000 });
        try {
            await page.click('ytcp-button#next-button', { timeout: 5000 });
        }
        catch {
            
        }
        finally {
            
        }

        try {
            await page.click('tp-yt-paper-radio-button[name="UNLISTED"]');
            await page.click('#done-button');
        }
        catch(e) {
            console.log(e);
            if (videoURL) {
                const id = videoURL.split('/').at(-1)?.trim();
                await statusFixer.SetDraftToUnlisted(`https://studio.youtube.com/video/${id}/edit`, page);
            }
        }
        await page.waitForTimeout(30000);
        addRecord(filepath, videoURL);
        await context.close();
        await page.close();
        return true;
    }
    catch(e: unknown) {
        await context.close();
        await page.close();
        console.log(e);
        return false;
    }
}
        
function moveFileToOtherDirectory(file: string, directory: string) {
    rename(`${DIR}${file}`, `${directory}${file}`).then(() => {
    });
}

(async () => {
    browser = await firefox.launch({ headless: false  });
    context = await browser.newContext();
    page = await context.newPage();
    // try {
    //     await login(page, STUDIO_CHANEL_URL, LOGIN, PASSWORD);
    //     context.storageState({ path: "state.json" });
    // }
    // catch (e: unknown) {
    //     await page.goto(STUDIO_CHANEL_URL);
    //     console.log(e);
    //     return;
    // }

    await page.close();
    let isLoopAlreadyRunning = false;

    /*
        Reads the watched UPLOAD directory, checks if the files present in the directory have been uploaded previously.
        To check this, we read the cache and the *downloaded.json* file, if the given filename is present in one of these, it means
        it has previously been uploaded to YouTube.

        A loop runs until all of the files are uploaded. If there was an error while uploading a file, it will be added to the queue
        in order to process it later.
    */
    async function readDirectory() {
        while (true) {
            let files = await readdir(DIR);
            if (!isLoopAlreadyRunning) {
                isLoopAlreadyRunning = true;

                if (files.length === 0) {
                    isLoopAlreadyRunning = false;
                }

                // Queueing
                for await (const file of files) {
                    if (!(await isInAirTable(file)) && !(await isInAirTable(file, "EditedVideos"))) {
                        console.log(`Uploadowanie: ${file}`);
                        const results = await uploadVideo(file);
                        if (results) {
                            console.log("Upload gotowy...");
                            moveFileToOtherDirectory(file, 'Z:/YouTube/AutoUpload Wstawione/')
                        } else {
                            console.log("Z jakiegoś powodu nie udało się zuploadować... Pomijamy na razie...")
                        }
                        continue;
                    } 
                }
                isLoopAlreadyRunning = false;
            }

            return;
        }
}

    await readDirectory();
    return;
})();
