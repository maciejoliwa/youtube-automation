import { Page } from 'playwright';

export default async function login(
    page: Page,
    STUDIO_CHANEL_URL: string,
    identifier: string | undefined,
    password: string | undefined
) {
    if (identifier === undefined || password === undefined) {
        throw new Error("Nie udało się załadować zmiennych środowiskowych");
    }

    await page.goto(STUDIO_CHANEL_URL);
    await page.type('input[type=email]', identifier, { timeout: 50000000 });
    await page.click('#identifierNext button');
    if (password !== '') {
        await page
            .waitForSelector('input[type=password]')
            .then((e) => e.type(password));
        await page.click('#passwordNext button');
    }
    await page.waitForURL(STUDIO_CHANEL_URL, { timeout: 5 * 60 * 1000 });
}
