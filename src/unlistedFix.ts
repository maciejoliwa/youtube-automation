import { ElementHandle, Page } from "playwright";

export class UnlistedStatusFixer {

    private async IsDraft(url: string, page: Page): Promise<boolean> {
        await page.waitForTimeout(5000);

        const buttons = await page.$$('.ytcp-button');

        return buttons.filter(async x => (await x.textContent()) === 'Edytuj wersję roboczą').length > 0;
    }

    private async GetDraftEditButton(page: Page): Promise<ElementHandle | null> {
        let buttons = await page.$$('.ytcp-button');
        buttons = buttons.filter(async x => (await x.textContent())?.includes('Edytuj wersję roboczą'));
        let btn;
        for (const button of buttons) {
            if ((await button.textContent())?.includes('Edytuj wersję roboczą')) {
                btn = button;
                break;
            }
        }
       
        // @ts-ignore
        return btn;
    }

    public async SetDraftToUnlisted(url: string, page: Page) {
        await page.goto(url);
        
        if (!(await this.IsDraft(url, page))) {
            return;
        } 

        (await this.GetDraftEditButton(page))?.click();
        await page.waitForTimeout(5000);
        await page.waitForSelector('#step-badge-3');
        await page.click('#step-badge-3');
        await page.waitForSelector('tp-yt-paper-radio-button[name="UNLISTED"]')
        await page.click('tp-yt-paper-radio-button[name="UNLISTED"]');
        await page.waitForSelector('#done-button');
        await page.click('#done-button');
    } 

}