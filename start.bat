@echo off
color 0a
SET /A XCOUNT=0

:loop

SET /A XCOUNT+=1

set mydate=%date:/=%
set mytime=%time::=%
set mytimestamp=%mydate%::%mytime%
echo %mytimestamp%  Eloszka, startujemy %XCOUNT% raz!
timeout /t 5

start Powershell.exe -ExecutionPolicy Unrestricted -command "npm --max-old-space-size=12288 start"

set mydate=%date:/=%
set mytime=%time::=%
set mytimestamp=%mydate%::%mytime%
echo %mytimestamp% Czekam 3 godzinki
timeout /t 10800

set mydate=%date:/=%
set mytime=%time::=%
set mytimestamp=%mydate%::%mytime%
echo %mytimestamp% echo Restartuje Apke
taskkill /IM "node.exe" /F

timeout /t 5
IF "%XCOUNT%" == "100" (
  GOTO end
) ELSE (
  GOTO loop
)


:end
pause
