// import test from 'node:test';
import { isInAirTable, addRecord } from '../src/airtable';

describe("Testing airtable records", () => {
    test("This record does not exist", async () => {
        const results = await isInAirTable("NON-EXISTENT_FILENAME213");
        expect(results).toBeFalsy();
    });
    test("This record does not exist 2", async () => {
        const results = await isInAirTable("NON-EXISTENT_FILENAME213", "EditedVideos");
        expect(results).toBeFalsy();
    });
    test("This record should exist", async () => {
        const results = await isInAirTable("TESTDONTREMOVE");
        expect(results).toBeTruthy();
    });
});